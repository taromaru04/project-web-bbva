# Proyecto Practitioner BBVA

Este API esta desarrollado para simular la banca por internet, en el cual el cliente puede solicitar una cuenta del bbva,
iniciar sesion y consultar sus movimientos integrando servicios rest con Reniec y envio de mensajes por whatapp para
que asi la comunicación con el cliente sea mas directa.

El código esta alojado en el siguiente repositorio bitbucket:

## Casos de Uso
### BACKEND
### GET:
**api= http://localhost:3002/api-peru/v1

- **Usuarios**
	- **api/users10** -> Busca todos los usuarios registrados dentro de la banca por internet y te devuelve todos sus datos por usuario(datos usuario, tarjeta, cuentas).

	- **api/people/:dni** -> Busca el dni de la persona a registrar y retorno los nombres, apellido paterno y materno con la finalidad de que en el registro no se ingrese un dato mall escrito.

- **Movimientos**
	- **api/movimientos/:idCuenta** -> Busca todo los movimientos de una cuenta por el idCuenta y retorna un array con todos sus movimimentos asociados.

	- **api/movNumCuenta/:numCuenta** -> Busca todo los movimientos de una cuenta por el número de cuenta y retorna un array con todos sus movimimentos asociados.

### POST:

- **Usuarios**
	- **api/usuarios2** -> Registra una persona como un cliente BBVA y retorna un json con todos los datos ingresados.
    Adicionalmente envia un correo con las credenciales para poder acceder a la banca por internet.

	- **api/login** -> Loguea un usuario agregando un elemento logged: true y retorna nombres, apellidos y dni.

	- **api/logout** -> Cierra sesión de un usuario eliminando el elemento logged:"" y retorna nombres, apellidos.

- **Movimientos**
	- **api/movimientos2** -> Registra el pago de la tarjeta, para esto busca el número de cuenta dentro de la colección usuarios para obtener el idCuenta y poder registrar el movimiento, tomando como idMovimiento (idMovimiento+1).

### PUT:

- **Usuarios**
	- **api/usuarios/:dni'** -> Actualiza el número celular buscando el dni dentro de la colección usuario y envia un mensaje por whatapp con el mensaje se actualizó correctamente número celular.

### DELETE:

- **Usuarios**
	- **api/usuarios/:dni'** -> Elimina un cliente con el dni (documento) y retorna mensaje "cliente eliminado".


## Lista de comandos para puesta en marcha Backend
 `node server.js 		 //con esto activamos el back con el puerto 3002.

## Requisitos "Adicionales" implementados

	- **https://dniruc.apisperu.com/api/v1/dni/'+dni+'?'+apikeyReniec** -> Se usa esta api para obtener los datos reales de la persona
	que desea registrarse a la web. Esta api es gratuita.

	- **https://eu44.chat-api.com/instance137189/** -> Se usa esta api para enviar mensaje al whatapp de la persona que esta editando
	su número de celular. Esta api es gratuita.

	- **nodemailer** -> Se usa este modulo de node para el envío por correo de las credenciales y posterior logueo del cliente.
